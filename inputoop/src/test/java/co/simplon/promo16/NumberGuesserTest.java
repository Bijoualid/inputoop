package co.simplon.promo16;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class NumberGuesserTest {

    NumberGuesser guesser;

    @Before
    public void init() {
        guesser = new NumberGuesser();
    }



    @Test
    public void shouldSayGreater() {
        assertEquals("Le nombre est trop grand", guesser.guess(8));
    }


    @Test
    public void shouldSayLesser() {
        assertEquals("Le nombre est trop petit", guesser.guess(2));
    }


    @Test
    public void congratulation() {
        assertEquals("Bravo c'était la bonne réponse", guesser.guess(5));
    }
}
