package co.simplon.promo16;

import java.util.Scanner;

public class NumberGuesser {

    private int goodAnswer = 5;
    private boolean isOver;



    public String guess(int userAnswer) {
    
        if (goodAnswer < userAnswer) {
            
            return "Le nombre est trop grand";
        }
    
        if (goodAnswer > userAnswer) {
    
            return "Le nombre est trop petit";
        }
        
        isOver = true;
        return "Bravo c'était la bonne réponse";
    }


    public void run() {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Essayez de trouver la bonne réponse: ");

        while (!isOver) {

            int input = scanner.nextInt();
            String feedback = guess(input);
            System.out.println(feedback);
            // scanner.next();
            

        }
        scanner.close();
    }
}



