package co.simplon.promo16.ToDo;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ScannerToDoList {
    private ToDoList toDoList;
    private boolean newTask;



    public ScannerToDoList() {
        this.toDoList = new ToDoList();
    }



    public void start() {
        Scanner scanner = new Scanner(System.in);

        while (!newTask) {

            System.out.println("Quelle tâche voulez vous rajouter ?");

            String input = scanner.nextLine();
            
            toDoList.addTask(input);

            System.out.println(toDoList.display()); 

        } scanner.close();

        
    }

}

/*
 * 1. Créer une classe ScannerTodoList qui aura en propriété une TodoList
 * (initialisée dans le constructeur par exemple) et une méthode start()
 */

/*
 * 2. Dans la méthode start, créer un scanner et faire une boucle comme on avait
 * fait dans la méthode play du NumberGuesser
 */

/*
 * 3. À l'intérieur de cette boucle, on va afficher avec un sysout
 * "Enter a new task" puis faire un nextLine pour récupérer un input
 * utilisateur·ice et utiliser cette input dans la méthode addTask de la
 * TodoList. Afficher ensuite le résultat avec la méthode display
 */