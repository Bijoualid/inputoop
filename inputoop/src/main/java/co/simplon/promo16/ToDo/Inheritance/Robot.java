package co.simplon.promo16.ToDo.Inheritance;

import java.util.UUID;

public class Robot {
    private int battery = 100;
    private String serialNumber;

    public Robot(int battery) {
        this.battery = battery;
    }

    public Robot() {
        this.serialNumber = UUID.randomUUID().toString();
    }

    public int charge(int value) {

        this.battery += value;
        if (battery > 100) {
            battery = 100;
        }

        return battery;
    }

    protected void discharge(int value) {
        this.battery -= value;

        if (battery < 0) {
            this.battery = 0;
        }

        System.out.println("Voici la batterie restance :" + battery);
    }


    public Boolean isOn() {
        if (battery > 0) {
            return true;
        } return false;
    }
}

/*
 * 3. Dans la classe Robot, rajouter une méthode charge(int value) qui va
 * augmenter la battery du nombre donné
 */

/*
 * 4. Créer dans le Robot une méthode protected int discharge(int value) qui va
 * baisser la battery de la value, et la repasser à 0 si jamais elle tombe en
 * dessous de zéro
 */

/*
 * 5. Faire une méthode isOn qui renverra true si la battery est supérieure à 0
 * et false sinon, puis faire que le cleaningBot ne fasse clean que s'il est
 * allumé
 */