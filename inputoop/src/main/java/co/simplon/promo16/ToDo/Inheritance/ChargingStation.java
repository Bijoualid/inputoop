package co.simplon.promo16.ToDo.Inheritance;

public class ChargingStation {
    private int power = 22;

    Robot robot = new Robot();

    public void connect(Robot robot) {

        robot.charge(power);
    }

    /*
     * 4. Dans la classe ChargingStation, rajouter une méthode connect(Robot) qui va
     * déclencher la méthode charge du robot en argument et charger sa battery de la
     * valeur de power
     */
}
