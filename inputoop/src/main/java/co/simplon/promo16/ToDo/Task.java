package co.simplon.promo16.ToDo;

import java.util.UUID;

public class Task {
    private String uuid;;
    private String label;
    private boolean done;

    public Task(String label) {
        this.uuid = UUID.randomUUID().toString();
        this.label = label;
        this.done = false;
    }

    public void toggleDone() {

        done = !done;
    }

    @Override
    public String toString() {
        return uuid + ", label: " + label + ", done: " + done + "\n";
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public boolean isDone() {
        return done;
    }


    public String display() {
        
        if (done != true) {
            return "☐ " + label;
        }
        return "☒ " + label;
    }

    /*
     * 1. Créer dans la classe Task une méthode display() qui renverra une chaîne de
     * caractère avec "☐ Label" dedans si la Task n'est pas done, et "☒ Label" si la
     * Task est done
     */




    /*
     * 3. Créer un constructeur dans la Task qui attendra un label et initialisera
     * le done à false et mettra un uuid à l'intérieur de la propriété du même nom.
     * Pour ce faire, vous pouvez utilisez la méthode randomUUID() de la classe
     * java.util.UUID
     */

}
