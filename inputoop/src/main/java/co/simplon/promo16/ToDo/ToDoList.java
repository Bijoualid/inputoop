package co.simplon.promo16.ToDo;

import java.util.ArrayList;
import java.util.List;

public class ToDoList {
    private List<Task> tasks;

    public ToDoList() {
        this.tasks = new ArrayList<>();
    }

    public void addTask(String label) {
        Task newTask = new Task(label);
        tasks.add(newTask);
    }

    public void removeTask(int index) {

    }

    public void clearDone() {

        for (Task taskBoucle : tasks) {
            
            if (taskBoucle.isDone() == true) {
                tasks.remove(taskBoucle);
            }
        }
    }

    public void getList() {
        System.out.println(tasks);
    }


    public void actionTask(int index) {
        tasks.get(index).toggleDone();
    }

    public String display() {
        String todoListString = "";
        for (Task task : tasks) {
            todoListString += task.display() +"\n";
        }
        return todoListString;
    }



    /*2. Dans la classe TodoList, créer une autre méthode display qui va :
   * Boucler sur chaque Task contenu dans la liste
   * A chaque tour de boucle, déclencher la méthode display de la Task
   * Rajouter un ptit saut de ligne à la fin de chaque tâche ("\n" le saut de ligne)
   * /

    /*
     * 4. Dans la méthode addTask de la TodoList, utiliser le paramètre label pour
     * créer une nouvelle instance de Task puis l'ajouter dans la liste de tasks
     */

    /*
     * 5. Dans la méthode clearDone(), faire une boucle sur toutes les tasks et
     * retirer de la liste celles dont le done est à true
     */

}
